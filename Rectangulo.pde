class Rectangulo {   
  int id;
  float x_pos, y_pos;
  PImage imagen;
  Rectangulo(int P_id, float P_xp, float P_yp, PImage f) {
    id = P_id;
    x_pos = P_xp;
    y_pos = P_yp;
    imagen=f;
  }  

  void storePos(float P_xp, float P_yp) {
    x_pos = P_xp;
    y_pos = P_yp;
  }

  int pieceNumber() {
    return id;
  }

  float xPosition() {
    return x_pos;
  }

  float yPosition() {
    return y_pos;
  }

  void design() {
    noStroke();
    if (id == cant_rectangulos-1) fill(color_fondo);   
    rect(x_pos+1, y_pos+1, long_rect-1, long_rect-1);
    if (id != cant_rectangulos-1) {
      image(imagen, x_pos-2,y_pos-2);

    }
  }
}