import android.view.MotionEvent;
import ketai.ui.*;

KetaiGesture gesture;
int z, t, p, id_rect;
int num_filas=3;
int long_rect=250;
int cant_rectangulos = 9; 
Rectangulo[] rectangulo_lista = new Rectangulo[cant_rectangulos];
color piece_color = color(255, 175, 0);
color color_fondo = color(235, 231, 178);
PImage foto;
PImage imagen[];
boolean tap=false;

float Tapx; 
float Tapy;
void setup() { 
  size(750, 750);
  background(0,0,0);
  //KETAI
  gesture = new KetaiGesture(this);
  //
  foto = loadImage("perro.jpg");
  imagen = new PImage[9];


  PVector[] xy_values = new PVector[cant_rectangulos];
  for (int i = 0; i < cant_rectangulos; i += num_filas) {
    for (int j = 0; j < num_filas; j++) {
      xy_values[z] = new PVector();
      xy_values[z].x = j*long_rect;
      xy_values[z].y = t*long_rect; 
      z++;
    }
    t++;
  } 

  int[] place = new int[cant_rectangulos];
  for (int i = 0; i < cant_rectangulos; i++) place[i] = 0;
  id_rect = 0;

  while (id_rect < cant_rectangulos) { 
    p = int(random(0, cant_rectangulos));
    if (place[p] == 0) { 
      rectangulo_lista[id_rect] = new Rectangulo(id_rect, xy_values[p].x, xy_values[p].y, generarPieza(id_rect));
      place[p] = 1;
      rectangulo_lista[id_rect].design();
      id_rect++;
    }
  }
}

public PImage generarPieza(int num){
  int i,j,k=0;
  for(i=0;i < 3 ; i++){
    for(j=0;j < 3; j++){
      imagen[k]= foto.get(j * 250, i * 250,250,250);
      k++;
    }
  }
  return imagen[num];
}


void draw() {    
  for (int i = 0; i < cant_rectangulos; i++) {
    if ( tap == true && Tapx >= rectangulo_lista[i].GetX() && Tapx <= rectangulo_lista[i].GetX()+long_rect && Tapy >= rectangulo_lista[i].GetY() && Tapy <= rectangulo_lista[i].GetY()+long_rect && rectangulo_lista[i].pieceNumber() != 8) {  
      if (Mover(rectangulo_lista[cant_rectangulos-1].GetX(), rectangulo_lista[cant_rectangulos-1].GetY(), rectangulo_lista[i].GetX(), rectangulo_lista[i].GetY())) {
        float temp_x = rectangulo_lista[cant_rectangulos-1].GetX(); 
        float temp_y = rectangulo_lista[cant_rectangulos-1].GetY();
        rectangulo_lista[cant_rectangulos-1].guardarPosicion(rectangulo_lista[i].GetX(), rectangulo_lista[i].GetY());
        rectangulo_lista[i].guardarPosicion(temp_x, temp_y);
        rectangulo_lista[cant_rectangulos-1].design(); 
        rectangulo_lista[i].design();     
      }
    }
  }
}

void onTap(float AuxTapx, float AuxTapy)
{
 tap=true;
 Tapx=AuxTapx;
 Tapy=AuxTapy;
 draw();
}


boolean Mover(float finalX, float finalY, float actualX, float actualY) {
    if (actualX == finalX && (actualY == finalY-long_rect || (actualY == finalY+long_rect))){
      return true;
    }
  else if (actualY == finalY && (actualX == finalX-long_rect || (actualX == finalX+long_rect))){ 
      return true;
        }
  else{
    return false;
  }
}


public boolean surfaceTouchEvent(MotionEvent event) {
  super.surfaceTouchEvent(event);
  return gesture.surfaceTouchEvent(event);
}